package com.example.rupeek.controller;

import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.rupeek.entity.Feeder;
import com.example.rupeek.entity.LoadCell;
import com.example.rupeek.entity.Motar;
import com.example.rupeek.service.FeederService;

@RestController
public class FeederController {
	@Autowired
	FeederService feederService;
	
	private static final Logger LOG = Logger.getLogger(FeederController.class.getName());
	
	/*
	 * Adding api to add material to feeder and start the sequence.
	 */
	@PostMapping(value="/feeder")
	public String addMaterialFeeder() {
			String mt = "Material";
			Random ran = new Random();
			int x = ran.nextInt(60) + 5;
			mt = mt + "_" +x;
			LOG.info("Start Adding material " + mt + " to feeder......");
			feederService.addMaterialToFeeder(mt);
			return mt;
	}
}
