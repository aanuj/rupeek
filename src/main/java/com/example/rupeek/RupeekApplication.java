package com.example.rupeek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RupeekApplication {

	public static void main(String[] args) {
		SpringApplication.run(RupeekApplication.class, args);
	}
}
