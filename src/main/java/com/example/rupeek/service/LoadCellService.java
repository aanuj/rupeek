package com.example.rupeek.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;
import com.example.rupeek.entity.Feeder;
import com.example.rupeek.entity.LoadCell;
import com.example.rupeek.entity.Motar;

@Service
public class LoadCellService {
	
	private static final Logger LOG = Logger.getLogger(LoadCellService.class.getName());
	
	public void dropMaterialToCellFromFeeder(Feeder feeder) {
		String dropingMaterial = feeder.dropMaterial();
		LOG.info("Droping material " + dropingMaterial +" to load cell " + feeder.getLc().getName());
		feeder.getLc().addMaterial(dropingMaterial);
		LOG.info("Material loaded to load cell " + feeder.getLc().getName());
		if (feeder.getLc().getM().getStatus().toLowerCase().equals("start")) {
			LOG.info("Motar " + feeder.getLc().getM().getName() + " is already running");
		} else {
			LOG.info("Staring Motar " + feeder.getLc().getM().getName());
			feeder.getLc().getM().setStatus("start");
			LOG.info("Motar"+ feeder.getLc().getM().getName() + " started");
		}
		try {
			Thread.sleep(20 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (feeder.getLc().getLoadCell() == null) {
			Motar m = new Motar();
			m.setName("motar 2");
			LoadCell loadCell2 = new LoadCell(m);
			loadCell2.setName("Load Cell 2");
			feeder.getLc().setLoadCell(loadCell2);
		}
		dropMaterialToCellFromCell(feeder.getLc());
	}
	
	public void dropMaterialToCellFromCell(LoadCell loadCell) {
		String m  = loadCell.dropMaterial();
		LOG.info("Drop material " + m + " from load cell " + loadCell.getName() +  " to " + loadCell.getLoadCell().getName());
		loadCell.getLoadCell().addMaterial(m);
		if (loadCell.getLoadCell().getM().getStatus().toLowerCase().equals("start")) {
			LOG.info("Motar " + loadCell.getLoadCell().getM().getName() + " is already running");
		} else {
			LOG.info("Staring Motar " + loadCell.getLoadCell().getM().getName());
			loadCell.getLoadCell().getM().setStatus("start");
			LOG.info("Motar"+ loadCell.getLoadCell().getM().getName() + " started");
		}
		LOG.info("Motar " + loadCell.getLoadCell().getName() + "  Started");
	}
	
	
}
