package com.example.rupeek.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;
import com.example.rupeek.entity.Feeder;
import com.example.rupeek.entity.LoadCell;
import com.example.rupeek.entity.Motar;

@Service
public class FeederService {
	
	Feeder feeder;
	
	@Autowired
	LoadCellService loadCellService;
	
	private static final Logger LOG = Logger.getLogger(FeederService.class.getName());

	public void addMaterialToFeeder(String mt) {
		// if feeder is null, create a feeder object with a motar and loadcell attached to it.
		if (feeder == null) {
			Motar m  = new Motar();
			m.setName("Motar 3");
			m.setStatus("stop");
			Motar m1  = new Motar();
			m1.setName("Motar 1");
			m1.setStatus("stop");
			LoadCell lc = new LoadCell(m1);
			lc.setName("LoadCell 1");
			feeder = new Feeder(m,lc);
			feeder.setName("Feeder");
		}
		feeder.addMaterial(mt);
		LOG.info("material " + mt +" gets added to "+ feeder.getName() +" ......");
		if (feeder.getM().getStatus().toLowerCase().equals("start")) {
			LOG.info("Motar " + feeder.getM().getName()  +" already in running status");
		} else {
			LOG.info("Starting motar " +  feeder.getM().getName());
			feeder.getM().setStatus("start");
			LOG.info("Motar " + feeder.getM().getName()  +" started");
		}
		loadCellService.dropMaterialToCellFromFeeder(feeder);
	}
}
