package com.example.rupeek.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class Feeder {
		List<String> mt;
		Motar m ;
		LoadCell lc;
		String name;

		public Feeder(Motar m, LoadCell lc) {
			this.mt = new ArrayList<String>();
			this.m = m;
			this.lc=lc;
		}
		
		public Motar getM() {
			return m;
		}
		public void setM(Motar m) {
			this.m = m;
		}
		public LoadCell getLc() {
			return lc;
		}
		public void setLc(LoadCell lc) {
			this.lc = lc;
		}
		public List getMt() {
			return mt;
		}
		public void setMt(List mt) {
			this.mt = mt;
		}
		public void addMaterial(String m) {
			mt.add(m);
		}
		
		public String dropMaterial() {
			return mt.remove(0);
		}
		public boolean isEmpty() {
			if (mt.size() == 0) {
				return true;
			}
			return false;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
}
