package com.example.rupeek.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class LoadCell {
	List<String> mt;
	Motar m ;
	LoadCell loadCell;
	String name;

	public LoadCell(Motar m) {
		this.mt = new ArrayList<String>();
		this.m = m;
	}
	public LoadCell getLoadCell() {
		return loadCell;
	}
	public void setLoadCell(LoadCell loadCell) {
		this.loadCell = loadCell;
	}
	public List getMt() {
		return mt;
	}
	public void setMt(List mt) {
		this.mt = mt;
	}
	public void addMaterial(String mat) {
		mt.add(mat);
	}
	
	public String dropMaterial() {
		if (mt.isEmpty()) {
			return "";
		}
		return mt.remove(0);
	}
	public boolean isEmpty() {
		if (mt.size() == 0) {
			return true;
		}
		return false;
	}
	
	public Motar getM() {
		return m;
	}
	public void setM(Motar m) {
		this.m = m;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
