package com.example.rupeek.scripts;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.example.rupeek.entity.Feeder;
import com.example.rupeek.service.LoadCellService;

@Component
public class MotorStatusVerification {
	
	@Autowired
	Feeder feeder;
	
	private static final Logger LOG = Logger.getLogger(MotorStatusVerification.class.getName());
	
	public void run() {
		LOG.info("Script to check feeder and loadCeller and change the status of motor accordingly");
		if (feeder == null) {
			LOG.info("No Feeder is present");
		}
		if (feeder.isEmpty()) {
			feeder.getM().setStatus("stop");
			LOG.info("Motor " + feeder.getM().getName() + " is stoped");
		}
		if (feeder.getLc().isEmpty()) {
			feeder.getLc().getM().setStatus("stop");
			LOG.info("Motor " + feeder.getLc().getM().getName() + " is stoped");
		}
		if (feeder.getLc().getLoadCell() != null && feeder.getLc().getLoadCell().isEmpty()) {
			feeder.getLc().getLoadCell().getM().setStatus("stop");
			LOG.info("Motor " + feeder.getLc().getLoadCell().getM().getName() + " is stoped");
		}
	}
	
	public static void main(String args[]){
		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		
		MotorStatusVerification job = context.getBean(MotorStatusVerification.class);
		
		job.run();
	}
}
