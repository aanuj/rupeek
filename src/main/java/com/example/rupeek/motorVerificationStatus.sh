#!/bin/bash

source /home/ubuntu/env_variable/init.sh

java -Duser.timezone=Asia/Calcutta -Dconfigpath=/home/ubuntu/adhoc-jobs/conf -Dloghome=$KSL_LOG_HOME -Dmode=$KSL_MODE -cp /home/ubuntu/adhoc-jobs/lib/*:/home/ubuntu/adhoc-jobs/conf/*:. com.example.rupeek.scripts.MotorStatusVerification